(function ($) {
    Drupal.behaviors.randomImageFormatter = {
        attach: function() {
            $('.random-image-formatter').once().each(function () {
                // Extract the image array from the data attribute, select a random one,
                // and insert an image into the DOM.
                var images = $(this).data('images');
                var image = images[Math.floor(Math.random()*images.length)];
                var $image = $('<img />');
                $image.attr('src', image.src);
                $image.attr('alt', image.alt);
                $(this).append($image);
            });
        }
    }
})(jQuery);
